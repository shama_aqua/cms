<?php
include "includes/header.php";
?>

<?php
include "includes/navbar.php";
//echo phpinfo();
?>

    <div class="row">
        <!-- main blog content  -->

<div class="col l9 m9 s12">
<?php
if(isset($_GET['id']))
{
    $id = $_GET['id'];
    $id = mysqli_real_escape_string($conn,$id);
    $id = htmlentities($id);
    $sql = "select * from posts where id=$id";

    $res = mysqli_query($conn,$sql);
    if(mysqli_num_rows($res)>0)
    {
        $sql2 = "update posts set views = views +1 where id=$id";
        $res2 = mysqli_query($conn,$sql2);

        $row = mysqli_fetch_assoc($res);
        $title = $row['title'];
        $image = $row['feature_image'] ?: 'no-image.png';
        $content = html_entity_decode($row['content']);
        $views = $row['views'];
?>
<div class="card-panel">
<h5 class="center"><?php echo ucwords($title);?></h5>

<card-image>
<img src="img/<?php echo $image;?>" alt="" style="max-width:100%;">
</card-image>
<p class="flow-text">
<?php echo $content;?>

</p>
<div class="row">
<div class="card-panel">

<div class="row">
<div class="col l8 offset-l2 m10 offset-m1 s12">
<h5>Write Comment</h5>
<?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
<form action="post.php?id=<?php echo $id; ?>" method="post">
<div class="input-field">
<input type="email" name="email" class="validate" placeholder="Enter Email">
<label for="email" data-error="Invalid Email Format"></label>
</div>
<div class="input-field">
<textarea name="comment" class="materialize-textarea" placeholder="Your comment goes here..."></textarea>
</div>
<div class="center">
<input type="submit" value="Comment" name="submit" class="btn">

</div>
</form>
<h5>Comments</h5>
<ul class="collection">
<?php
$sql4 = "select * from comment where post_id=$id and status= 1 order by id desc limit 10";
//echo $sql4;
$res4 = mysqli_query($conn,$sql4);
if(mysqli_num_rows($res4)>0)
{
    
    while($row = mysqli_fetch_assoc($res4))
    {
    $email = $row['email'];
    $comment = $row['comment_text'];
    
    
?>
<li class="collection-item">
<?php echo $comment; ?>
<span class="secondary-content"><?php echo $email; ?></span>
</li>
<?php
    }
}
?>


</ul>

</div>

</div>

</div>


<!-- related blog bar -->

<div class="col s12">
<h5>Related Blogs</h5>

<?php
$sql = "select * from posts where id!=$id order by rand() limit 4";

$res = mysqli_query($conn,$sql);
if(mysqli_num_rows($res)>0)
{
    
    while($row = mysqli_fetch_assoc($res))
    {
    $title = $row['title'];
    $image = $row['feature_image'] ?: 'no-image.png';
    
    
?>
<div class="col l3 m4 s6">
<div class="card">

<div class="card-image">
<div class="center">
<img src="img/<?php echo $image;?>" alt="">
<span class="card-title black-text truncate"><?php echo $title;?></span>
</div>
</div>
<div class="card-action teal center"><a href="post.php?id=<?php echo $row['id'];?>" class="white-text">Read More</a></div>
</div>

</div>
<?php 
    }
} ?>


</div>
</div>

</div>

<?php

    }
}
    else
    {
        header("Location: index.php");

    }


?>
</div>
<!-- side bar -->
<div class="col l3 m3 s12">
<?php include "includes/sidebar.php";?>
</div>


    </div>

    <?php
include "includes/footer.php";
?>
<?php
if(isset($_POST['submit']))
{
$email = $_POST['email'];
$comment = $_POST['comment'];
$id1  = $_GET['id'];

$email = mysqli_real_escape_string($conn,$email);
$comment = mysqli_real_escape_string($conn,$comment);
$id1 = mysqli_real_escape_string($conn,$id1);

$email = htmlentities($email);
$comment = htmlentities($comment);
$id1 = htmlentities($id1);

$sql3 = "insert into comment(email,comment_text,post_id) values('$email','$comment',$id1)";
$res3 = mysqli_query($conn,$sql3);
//echo $sql3;
if($res3)
{

    $_SESSION['message'] = "<div class='chip green white-text'>Comment added successfully.</div>";
    header("Location: post.php?id=$id1");
}
else
{
    $_SESSION['message'] = "<div class='chip red black-text'>Sorry, something went wrong.</div>";
    header("Location: post.php?id=$id1");
}

}


?>