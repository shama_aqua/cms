<?php
include "includes/header.php";
?>

<?php
include "includes/navbar.php";
?>
    <div class="row">
        <!-- main content -->
    <div class="col l9 m9 s12">
<?php
$sql = "select * from posts order by id desc";
$res = mysqli_query($conn,$sql);

if($res)
{
    if(mysqli_num_rows($res)>0)
    {
    
    while($row=mysqli_fetch_assoc($res))
    {
?>
<div class="col l3 m4 s6">
<div class="card small">

<div class="card-image">

<img src="img/<?php echo $row['feature_image']?:'no-image.png';?>" alt="">
<span class="card-title black-text truncate"><?php echo $row['title'];?></span>
</div>
<div class="card-content truncate"><?php echo html_entity_decode($row['content']);?></div>
<div class="card-action teal center"><a href="post.php?id=<?php echo $row['id'];?>" class="white-text">Read More</a></div>
</div>

</div>
<?php
    }
    }
}?>



</div>

<!-- side bar -->
<div class="col l3 m3 s12">
<?php include "includes/sidebar.php";?>
</div>
    </div>


     <?php
include "includes/footer.php";
?>