<footer class="page-footer teal">
<div class="container">
Theme developed by Shama
<span class="right">
<a href="" class="btb btn-floating white"><i class="fa fa-facebook blue-text"></i></a>
<a href="" class="btb btn-floating white"><i class="fa fa-youtube red-text"></i></a>
<a href="" class="btb btn-floating white"><i class="fa fa-twitter blue-text"></i></a>
<a href="" class="btb btn-floating white"><i class="fa fa-instagram red-text"></i></a>
</span>
</div>
<div class="clearfix"></div>
<div class="footer-copyright">

<div class="container">
&copy; All Rights Reserved, Blogera
</div>
</div>

</footer>


<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/jquery.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script>
      $(document).ready(function () {
        
        //alert("hello");
        $('.button-collapse').sideNav();

      });
      
      </script>
    </body>
  </html>