<?php
include "includes/header.php";

if(isset($_POST['login']))
{

$username=$_POST['username'];
$password=$_POST['password'];

$username = mysqli_real_escape_string($conn,$username);
$password = mysqli_real_escape_string($conn,$password);

$username = htmlentities($username);
$password = htmlentities($password);

//$password = password_hash($password, PASSWORD_BCRYPT);
$sql = "select * from users where username='$username'";
$res = mysqli_query($conn,$sql);
$row = mysqli_fetch_assoc($res);

//echo $row['password'];

if(password_verify($password,$row['password']))
{
    $_SESSION['username'] = $username;
    $_SESSION['email'] = $row['email'];
    header("Location: dashboard.php");
}
else
{
   $_SESSION['message'] = "<div class='chip red black-text'>Sorry, credentials don't match.</div>";
   header("Location: login.php");


}
}
?>