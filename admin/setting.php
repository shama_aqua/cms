<?php include "includes/header.php"; ?>
<?php include "includes/navbar.php"; ?>
<div class="main">
<div class="row">
<div class="card-panel">
<h5 class="center">Settings: Change Password</h5>
<?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
<form action="setting.php" method="post">
<input type="password" name="password" placeholder="Change Password">
<input type="password" name="con_password" placeholder="Confirm Password">
<div class="center">
<input type="submit" value="Change Password" class="btn" name="update">
</div>
</form>
</div>
</div>
</div>

<?php include "includes/footer.php"; ?>

<?php
if(isset($_POST['update']))
{

    $password=$_POST['password'];
    $password = mysqli_real_escape_string($conn,$password);
    $password = htmlentities($password);

    $con_password=$_POST['con_password'];
    $con_password = mysqli_real_escape_string($conn,$con_password);
    $con_password = htmlentities($con_password);

    if($password===$con_password)
    {
        $username = $_SESSION['username'];
        $password = password_hash($password, PASSWORD_BCRYPT);
        $sql = "update users set password='$password' where username='$username'";
        $res = mysqli_query($conn,$sql);
        if($res)
        {
            $_SESSION['message'] = "<div class='chip green white-text'>Passwords successfully changed.</div>";
        
        }
        else
        {
            $_SESSION['message'] = "<div class='chip red black-text'>Sorry, something went wrong. Try again.</div>";
        
        }
        header("Location: setting.php");
    }
    else
    {
  
        $_SESSION['message'] = "<div class='chip red black-text'>Sorry, passwords do not match.</div>";
        header("Location: setting.php");
    }


}

?>