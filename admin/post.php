<?php include "includes/header.php"; ?>
<?php include "includes/navbar.php"; ?>
<?php
if(!isset($_SESSION['username']))
{
  $_SESSION['message'] = "<div class='chip red black-text'>Login to continue.</div>";
  
  //echo "write - mess:" . $_SESSION['message'];
    header("Location: login.php");
}
?>
<div class="row main">
<div class="row">
<div class="col l12 m12 s12">
<div class="card-panel">

<!--start posts collection -->
<?php include "includes/post_collection.php"; ?>
<!-- end post collection -->

</div>

</div>
</div>

</div>


<div class="fixed-action-btn">
<a href="write.php" class="btn-floating btn btn-large white-text pulse">
<i class="material-icons">edit</i></a>
</div>
<?php include "includes/footer.php"; ?>