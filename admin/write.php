<?php include "includes/header.php"; ?>
<?php include "includes/navbar.php"; ?>
<?php
if(isset($_SESSION['username']))
{

?>
<div class="main">


<form action="write_check.php" method="post" enctype="multipart/form-data">
<div class="card-panel">
<?php
if(isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<h5>Title for Post</h5>
<textarea name="title" class="materialize-textarea" placeholder="Title for post"></textarea>
<h5>Featured Image</h5>
   <div class="input-field file-field">
<div class="btn">
Upload File
<input type="file" name="image">
</div>

<div class="file-path-wrapper">

<input type="text" name="" class="file-path" required>
</div>

   </div>


<h5>Post Content</h5>
<textarea name="ckeditor" id="ckeditor" class="ckeditor"></textarea>
<div class="center">
<br>
<input type="submit" value="Publish" name="publish" class="btn white-text">

</div>
</div>
</form>

</div>

<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
<?php include "includes/footer.php"; ?>

<?php
}
else
{

    $_SESSION['message'] = "<div class='chip red black-text'>Login to continue.</div>";

    //echo "write - mess:" . $_SESSION['message'];
    header("Location: login.php");
}
?>