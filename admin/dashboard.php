<?php include "includes/header.php"; ?>
<?php include "includes/navbar.php"; ?>
<?php
if(isset($_SESSION['username']))
{

?>
<div class="main">
<div class="row">
<div class="col l6 m6 s12">
<!--start posts collection -->
<?php include "includes/post_collection.php"; ?>
<!-- end post collection -->
</div>
<div class="col l6 m6 s12">

<ul class="collection with-header">
<li class="collection-header teal"><h5 class="white-text">Recent Comments</h5>
<span id="message_comment"></span>
</li>

<?php
$sql4 = "select * from comment order by id desc limit 10";
//echo $sql4;
$res4 = mysqli_query($conn,$sql4);
if(mysqli_num_rows($res4)>0)
{
    
    while($row = mysqli_fetch_assoc($res4))
    {
    $email = $row['email'];
    $comment = $row['comment_text'];
    
    
?>
<li class="collection-item">
<?php echo $comment; ?>
<span class="secondary-content"><?php echo $email; ?></span>
<br>
<span>

<a href="" class="approve" id="<?php echo $row['id']; ?>" status="<?php echo $row['status']; ?>">
<?php 
if($row['status']==="0")
{
?>
<i class="material-icons tiny green-text">done</i> Approve
<?php } ?>

<?php 
if($row['status']==="1")
{
?>
<i class="material-icons tiny red-text">clear</i> Remove
<?php } ?>
</a>

</span>
</li>
<?php
    }
}
?>
</ul>

</div>
</div>
</div>

<div class="fixed-action-btn">
<a href="write.php" class="btn-floating btn btn-large white-text pulse">
<i class="material-icons">edit</i></a>
</div>
<?php
}
else
{

    $_SESSION['message'] = "<div class='chip red black-text'>Login to continue.</div>";
    header("Location: login.php");

}
?>


<?php include "includes/footer.php"; ?>