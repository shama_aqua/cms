<ul class="collection with-header">
<li class="collection-header teal"><h5 class="white-text">Recent Posts</h5>
<span id="message"></span>
</li>
<?php
$sql = "select * from posts order by id desc";
$res = mysqli_query($conn,$sql);
if(mysqli_num_rows($res)>0)
{
    while($row=mysqli_fetch_assoc($res))
    {
?>
<li class="collection-item" id="listid<?php echo $row['id']; ?>">
<a href=""><?php echo $row['title']; ?>
</a>
<br>
<span><a href="edit.php?id=<?php echo $row['id']; ?>"><i class="material-icons tiny">edit</i> Edit</a> | <a href="" id="<?php echo $row['id']; ?>" class="delete"><i class="material-icons tiny red-text">clear</i>    Delete</a> | <a href=""><i class="material-icons tiny green-text">share</i> Share</a></span>
</li>
<?php
    }
}
else
{
 
    echo "<div class='center'><li class='collection-item'><div class='chip red white-text' style='height:auto;'>No posts yet, write a new post by clicking circular button at bottom right of screen.</div></li></div>";

}
?>
</ul>