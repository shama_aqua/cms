
<ul class="collection with-header">
<li class="collection-header teal"><h5 class="white-text">Recent Comments</h5>
<span id="message_comment"></span>
</li>

<?php
#PDO Query method
$stmt = $pdo->query("select * from comment order by id desc limit 10");

#PDO Prepare & Execute method

// $sql = "select * from comment where id = :id order by id desc limit 10";
// $stmt = $pdo->prepare($sql);
// $stmt->execute(['id' => '34']);

// $sql = "select * from comment order by id desc limit 10";
// $stmt = $pdo->prepare($sql);
// $stmt->execute([]);

    while($row = $stmt->fetch())
    {
    $email = $row['email'];
    $comment = $row['comment_text'];
    
    
?>
<li class="collection-item">
<?php echo $comment; ?>
<span class="secondary-content"><?php echo $email; ?></span>
<br>
<span>

<a href="" class="approve" id="<?php echo $row['id']; ?>" status="<?php echo $row['status']; ?>">

<?php 
if($row['status']==="0")
{
?>
<i class="material-icons tiny green-text">done</i> Approve
<?php } ?>

<?php 
if($row['status']==="1")
{
?>
<i class="material-icons tiny red-text">clear</i> Remove
<?php } ?>
</a>

</span>
</li>
<?php
    }

?>
</ul>
