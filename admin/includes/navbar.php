
<nav class="teal">
<div class="nav-wrapper">
<div class="container">
<a href="../" class="brand-logo center">Blogera</a>
<a href="" class="button-collapse show-on-large" data-activates="sidenav">
<i class="material-icons">menu</i>

</a>
</div>
</div>
</nav>

<ul class="side-nav fixed" id="sidenav">
<li>
<div class="user-view">
<div class="background">
<img src="../img/background.png" alt="" class="responsive-img">
</div>
<a href=""><img src="../img/img-profile.jpg" alt="" class="circle"></a>
<span class="name black-text"><?php echo $_SESSION['username']; ?></span>
<span class="email black-text"><?php echo $_SESSION['email']; ?></span>
</div>
</li>
<li><a href="dashboard.php">Dashboard</a></li>
<li><a href="post.php">Posts</a></li> 
<li><a href="image.php">Images</a></li>
<li><a href="comment.php">Comments</a></li>
<li><a href="setting.php">Settings</a></li>
<div class="divider"></div>
<li><a href="logout.php">Logout</a></li>
</ul>