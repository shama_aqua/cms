
<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="../js/jquery.js"></script>
      <script type="text/javascript" src="../js/materialize.min.js"></script>



      <script>
      $(document).ready(function () {
        $(".button-collapse").sideNav();
      });

      //delete posts
      const del = document.querySelectorAll(".delete");
      //console.log(del);
      del.forEach(function(item,index)
      {

        //console.log(item);
        item.addEventListener('click',deleteNow);
      }
      )
      function deleteNow(e)
      {
        e.preventDefault();
        if(confirm("Are you sure, you want to delete?"))
        {

            //console.log(e.target);
            const xhr = new XMLHttpRequest();
            xhr.open("GET","delete.php?id="+Number(e.target.id),true);
            xhr.onload = function()
            {
              const messg = xhr.responseText;
              const message = document.getElementById("message");
              message.innerHTML = messg;
             // console.log(messg);
  
              var element = document.getElementById("listid"+Number(e.target.id));
              element.parentNode.removeChild(element);

            }
            
            xhr.send();

        }

      }
//approve posts

const approve = document.querySelectorAll(".approve");
      //console.log(approve);
      approve.forEach(function(item,index)
      {

        //console.log(item);
        item.addEventListener('click',approveNow);
      }
      )
      function approveNow(e)
      {
        e.preventDefault();

        
        var element = document.getElementById(Number(e.target.id));
        var element_status = element.getAttribute("status");

        let conf_message;
        console.log(element);


        if(element_status === "0")
              {
                
                conf_message = "Are you sure, you want to approve?";
                
              }
              if(element_status === "1")
              {

                conf_message = "Are you sure, you want to remove?";
                
              }

        if(confirm(conf_message))
        {

            //console.log(e.target);
            const xhr = new XMLHttpRequest();
            xhr.open("GET","approve.php?id="+Number(e.target.id),true);
            xhr.onload = function()
            {
              const messg = xhr.responseText;
              const message = document.getElementById("message_comment");
              message.innerHTML = messg;
             // console.log(messg);
             

              if(element_status === "0")
              {

                //console.log("status"+element_status);
                element.setAttribute("status", "1");
 
                element.innerHTML = "<i class='material-icons tiny red-text'>clear</i> Remove";
                
              }
              if(element_status === "1")
              {

                //console.log("status"+element_status);
                element.setAttribute("status", "0");
                element.innerHTML = "<i class='material-icons tiny green-text'>done</i> Approve";
                
              }


              

            }
            
            xhr.send();

        }

      }


      </script>
    </body>
  </html>