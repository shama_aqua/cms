<?php include "includes/header.php"; ?>
<?php include "includes/navbar.php"; ?>

<?php
if(isset($_GET['id']))
{

    $id  = $_GET['id'];
    $id = mysqli_real_escape_string($conn,$id);
    $id = htmlentities($id);

    echo $id;
    $sql = "select * from posts where id =$id";
$res = mysqli_query($conn,$sql);

if(mysqli_num_rows($res)>0)
{ 
  $row = mysqli_fetch_assoc($res);  
?>

<div class="main">


<form action="edit_check.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
<div class="card-panel">
<?php
if(isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<h5>Title for Post</h5>
<textarea name="title" class="materialize-textarea" placeholder="Title for post">
<?php echo $row['title']; ?>
</textarea>
<h5>Featured Image</h5>
<div class="row">
<div class="col l8 m8 s12">
   <div class="input-field file-field">
<div class="btn">
Upload File
<input type="file" name="image" id="">
</div>

<div class="file-path-wrapper">

<input type="text" name="" class="file-path">
</div>

   </div>
</div>
<div class="col l4 m4 s12">
   <input type="hidden" name="old_file" value="<?php echo $row['feature_image'];?>">
   <img src="../img/<?php echo $row['feature_image']?:'no-image.png';?>" alt="" style="max-width:100%">
   
   </div>
   </div>

<h5>Post Content</h5>
<textarea name="ckeditor" id="ckeditor" class="ckeditor">
<?php echo $row['content']; ?>
</textarea>
<div class="center">
<br>
<input type="submit" value="Update" name="update" class="btn white-text">

</div>
</div>
</form>

</div>

<script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
<?php include "includes/footer.php"; ?>

<?php   
}
else
{

    header("Location: dashboard.php");
}

}

?>


