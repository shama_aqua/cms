<?php
include "includes/header.php";
if(isset($_SESSION['username']))
{
unset($_SESSION['username']);
//session_destroy();
$_SESSION['message'] = "<div class='chip green white-text'>You have been logged out</div>";
header("Location: login.php");

}
else
{
    $_SESSION['message'] = "<div class='chip red black-text'>Login to continue.</div>";
    header("Location: login.php");

}

?>