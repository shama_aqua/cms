<?php include "includes/header.php"; ?>
<?php //echo "message:" . $_SESSION['message']; 

if(!isset($_SESSION['username']))
{


?>

<body style="background-image:url('../img/img13.jpg');background-size:cover;">
    <div class="row" style="margin-top:50px;">
    <div class="col l6 offset-l3 m8 offset-m2 s12">
    <div class="card-panel center grey lighten-2" style="margin-bottom:0px;">
    <ul class="tabs grey lighten-2">
    <li class="tab"><a href="#login" class="black-text">Login</a></li>
    <li class="tab"><a href="#signup" class="black-text">Sign Up</a></li>
    
    </ul>
    
    </div>
    
    </div>
    <!-- Login and sign up area -->
    <div class="col l6 offset-l3 m8 offset-m2 s12" id="login">
    <div class="card-panel center red lighten-3" style="margin-top:1px;">
    <h5>Login To Continue</h5>
    <?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
    <form action="login_check.php" method="post">
    <div class="input-field">
    <input type="text" name="username" id="username" placeholder="Username" required>
    </div>
    <div class="input-field">
    <input type="password" name="password" id="password" placeholder="Password" required>
    </div>
    <input type="submit" value="Login" name="login" class="btn">
    </form>
    </div>
    
    </div>
    <div class="col l6 offset-l3 m8 offset-m2 s12" id="signup">
    <div class="card-panel center blue lighten-3"  style="margin-top:1px;">
    <h5>Sign Up Now</h5>
    <form action="signup.php" method="post">
    <div class="input-field">
    <input type="email" name="email" id="email" placeholder="Enter Email" class="validate" required>
    <label for="email" data-error="Invalid Email" data-success="Valid Email"></label>
    </div>
    <div class="input-field">
    <input type="text" name="username" id="username" placeholder="Username" required>
    </div>
    <div class="input-field">
    <input type="password" name="password" id="password" placeholder="Password" required>
    </div>
    
    <input type="submit" value="Sign Up" name="signup" class="btn">
    </form>
    </div>

    </div>

    </div>
<?php include "includes/footer.php"; 
}
else
{
    header("Location: dashboard.php");
}


?>